#!/usr/bin/perl

# Perl script om invoer van HTML bestelformulieren op te slaan en te verwerken
# Geschreven door Ferdi Scholten voor Bosvreugdevents, januari 2024
# modules: CGI - https://metacpan.org/pod/CGI
#          Path::Tiny - https://metacpan.org/pod/Path::Tiny
#          Data::Dumper - https://metacpan.org/pod/Data::Dumper
#          Locale::Currency::Format - https://metacpan.org/pod/Locale::Currency::Format
#          DateTime - https://metacpan.org/pod/DateTime

use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);
use Locale::Currency::Format;
use DateTime;
# use Data::Dumper qw(Dumper); # voor debugging om eenvoudig de inhoud van hashes etc. te bekijken

# variabelen declaratie
my %data;	                  # Hash voor ophalen gegevens webformulier
my @uitvoer;                  # inhoud van het weg te schrijven bestand
my @bestelling;               # lijst met de bestelling
my $path = "/mnt/data/data/bestellingen/"; # De lokatie waar de bestanden staan en/of worden opgeslagen
my $key;                      # naam van het ingevulde formulierveld in het bestelformulier en hoeveelheid bestelde artikelen (als $data{$key})
my $formname;                 # naam van het bestelformulier
my $dirname;                  # naam van de directory, gebaseerd op formname, zonder spaties
my $datum;                    # invuldatum van het onderzoek
my $code;                     # bestelcode van degene die de bestelling doet
my $name;                     # naam van degene die de bestelling doet
my $artikel;                  # artikelnaam
my $prijs;                    # artikelprijs
my $factor;                   # omrekenfactor (b.v. om subtotaal te berekenen bij invoer in gram n.a.v. prijs per kilo)
my $subtotaal;                # subtotaalprijs per artikel
my $totaal = 0;               # totaalbedrag van de bestelling
my $aantal = 0;               # teller voor aantal bestelde artikelen
my ( $eind_dag, $eind_uur, $week, $lever_week, $dag, $uur, $jaar, $i, $tijd );
my $info;
my $inforegel = '<p style="border: 1px solid black;"><strong>Opmerkingen bij bestelling:</strong><br>';
my $foutmelding = "<h2>Foutmelding</h2>
            <p>Er is een fout opgetreden bij de verwerking van uw gegevens, probeer het later opnieuw, neem contact op met Vrijdenkersplaats Bosvreugd als het dan nog niet gaat.</p>";
my $htmlheader = '<!DOCTYPE HTML>
		<html lang="nl">
		<head>
		<meta "charset=UTF-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<title>Melding</title>
		<link href="https://vrijdenkersplaats.nl/bestellingen/styles/bosvreugd_b.css" type="text/css" rel="stylesheet">
		</head>
		<style>
		table {border: 1px solid black;
		        border-collapse: collapse;}
		thead th, tbody td {border: 1px solid black;}
		.k1 {text-align: center;}
		</style>
		<body>
		<img class="logo" src="https://vrijdenkersplaats.nl/bestellingen/afbeeldingen/Bosvreugdlogo_transparant.png" title="Bosvreugd, waar vrijdenkers hun thuis hebben!" alt="Bosvreugd Logo" style="float: left; ">
		<h1 style="text-align: center;">Besteloverzicht</h1>';
my $htmlfooter = '<p>bestellingen script gemaakt door Ferdi Scholten</p></div>
		 </body></html>';

# open connectie voor data uitwisseling met website
my $q = CGI->new;
print $q->header;

# vul de hash %data met de gegevens van het formulier
foreach $key ($q->param()){    
	$data{$key} = $q->param($key);
	}
my $dt = DateTime->now;
$dt->set_time_zone('Europe/Amsterdam');
# validatie van ingevulde gegevens & samenstellen bestandsnaam : bestelcode-invuldatum.txt
$code = $data{'bestelcode'};
if ($code eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen bestelcode ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$formname = $data{'formset'};
if ($formname eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen formset ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$datum = $data{'invuldatum'};
if ($datum eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen invuldatum ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$name = $data{'naam'};
if ($name eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen naam ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$info = $data{'info'};            # indien extra informatie is ingevuld bij de bestelling wordt deze in $info geplaatst.
if ($info) {
     # vervang \n (newline) door <br>
     $i = index $info, "\n";
     while ($i != -1) {
     	my $test = substr $info, $i, 1, "<br>";
     	$i = index $info, "\n";
     }
     $inforegel = "$inforegel $info </p><hr>";
     $aantal = 1;
     } else {
     $inforegel = "$inforegel </p><hr>";
     }
$eind_dag = $data{'einddag'};     # dag waarop de bestelling binnen moet zijn om dezelfde week afgehaald te worden (1 - 7)
$eind_uur = $data{'einduur'};     # uur van de dag dat de bestelling binnen moet zijn om dezelfde week afgehaald te worden (0 - 23)
$jaar = $dt->year;                # huidig jaar
$dag = $dt->day_of_week;          # huidige dag (1 - 7)
$uur = $dt->hour;                 # huidig uur (0 - 23)
$tijd = $dt->hms('');             # huidige tijd (HHMMSS)
$week = $dt->week_number;         # huidige week (1 - 53)
$lever_week = $week;
if ($dag == $eind_dag) {
   if ($uur >= $eind_uur) {
   $lever_week = $week + 1;
   }
}
if ($dag > $eind_dag) {
   $lever_week = $week + 1;
}
my $bestand = "$code-$datum-$tijd";
$dirname = $formname;
$dirname =~ tr/ /_/;      #vervang spaties door _
$path = "$path$dirname/$jaar/$lever_week";
# Het array @bestelling wordt gevuld, hiermee wordt de uitvoer gecreërd naar browser en bestand
push @bestelling, '<p>Gedaan door: ';
push @bestelling, $name;
push @bestelling, '<br>op datum: ';
push @bestelling, $datum;
push @bestelling, ' week: ';
push @bestelling, $week;
push @bestelling, '<br> Uw bestelling kan opgehaald worden bij Bosvreugd op donderdagavond in week: ';
push @bestelling, $lever_week;
push @bestelling, '</p>
       <table>
       <colgroup>
       <col span="1" style="width: 15%">
       <col span="1" style="width: 55%">
       <col span="1" style="width: 15%">
       <col span="1" style="width: 15%">
       </colgroup>
       <thead>
       <tr><th>Aantal</th><th>Artikel</th><th>Prijs</th><th>Subtotaal</th></tr>
       </thead>
       <tbody>';
# Sorteer de hash en plaats de uitvoer in @uitvoer met een | teken achter key en value, dit is voor debugging en bevat alle data die is opgehaald
# De gesorteerde uitvoer wordt gebruikt om de tabel met bestelde artikelen te maken
foreach $key (sort keys %data){
     push @uitvoer, $key.'|'.$data{$key}.'|';
     if ($key =~ /.*([0-9])$/) {
       if ($data{$key} ne "" ){
       $artikel= $key.'_a';
       $prijs= $key.'_p';
       $factor=$key.'_f';
       $subtotaal = $data{$key} * $data{$prijs} / $data{$factor};
       $totaal = $totaal + $subtotaal;
       my $fprijs = currency_format('EUR', $data{$prijs}, FMT_HTML);
       my $fsubtotaal = currency_format('EUR', $subtotaal, FMT_HTML);
       my $rij = '<tr><td class="k1">'.$data{$key}.'</td><td class="k2">'.$data{$artikel}.'</td><td class="k3">'.$fprijs.'</td><td class="k4">'.$fsubtotaal.'</td></tr>';
       push @bestelling, $rij;
       $aantal = $aantal + 1;
       }
     }
}
if ($aantal == 0) {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen artikelen besteld</p>";
     print $htmlfooter;
     exit;
}
$totaal = currency_format('EUR', $totaal, FMT_HTML);
push @bestelling, '</tbody>
       <tfoot>
       <tr><td>Totaal:</td><td></td><td></td><td class="k4">';
push @bestelling, $totaal;
push @bestelling, '</td></tr>
       </tfoot>
       </table>';
push @bestelling, $inforegel;
# sla de @bestelling op in het bestand
my $location = "$path/$bestand.txt";
path($path)->mkdir;                   #creëer directory indien die nog niet bestaat
path($location)->touch;               #creëer het bestand indien het nog niet bestaat
path($location)->spew_utf8(@bestelling); # de inhoud van @bestelling word weggeschreven in het bestand

# creëer de webpagina om het resultaat weer te geven
print $htmlheader;
print '<h2>Succes!</h2>
       <p>Uw invoer is succesvol verwerkt, hartelijk dank voor uw bestelling</p>';
foreach my $regel (@bestelling) {
print $regel; }
print '<p>U kunt deze bestelling printen of opslaan door op onderstaande knop te klikken/tikken<br>
       <input type="button" value="Bestelling printen/opslaan" onClick="window.print()"></p>';
print $htmlfooter;       