#!/usr/bin/perl

# Perl script om opgeslagen bestellingen per week op te halen en in de browser te tonen
# Geschreven door Ferdi Scholten voor Bosvreugdevents, januari 2024
# modules: CGI - https://metacpan.org/pod/CGI
#          Path::Tiny - https://metacpan.org/pod/Path::Tiny
#          Data::Dumper - https://metacpan.org/pod/Data::Dumper

use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);
# use Data::Dumper qw(Dumper); # voor debugging om eenvoudig de inhoud van hashes etc. te bekijken

# variabelen declaratie
my %data;	                               # Hash voor ophalen gegevens webformulier
my @uitvoer;                               # inhoud van het weg te schrijven bestand
my @bestanden;                             # lijst met de bestanden met bestellingen
my @gesorteerd;                            # lijst met bestanden, gesorteerd op datum
my $path = "/mnt/data/data/bestellingen/"; # De lokatie waar de bestanden staan en/of worden opgeslagen
my $key;                                   # naam van het ingevulde formulierveld in het bestelformulier en hoeveelheid bestelde artikelen (als $data{$key})
my $formname;                              # naam van het bestelformulier
my $dirname;                               # naam van de directory, gebaseerd op formname, zonder spaties
my $data;                                  # voor inlezen inhoud van bestanden
my ( $week, $jaar );
my $foutmelding = "<h2>Foutmelding</h2>
            <p>Er is een fout opgetreden bij de verwerking van uw gegevens, probeer het later opnieuw, neem contact op met Bosvreugdevents als het dan nog niet gaat.</p>";
my $htmlheader = '<!DOCTYPE HTML>
		<html lang="nl">
		<head>
		<meta "charset=UTF-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<title>Uitvoer bestellingen</title>
		<link href="https://bosvreugdevents.nl/bestellingen/styles/bosvreugd_b.css" type="text/css" rel="stylesheet">
		</head>
		<style>
		table {border: 1px solid black;
		        border-collapse: collapse;}
		thead th, tbody td {border: 1px solid black;}
		.k1 {text-align: center;}
		</style>
		<body>
		<img class="logo" src="https://bosvreugdevents.nl/bestellingen/afbeeldingen/Bosvreugdlogo_transparant.png" title="Bosvreugd, waar vrijdenkers hun thuis hebben!" alt="Bosvreugd Logo" style="float: left; ">
		<h1 style="text-align: center;">Bestellingen overzicht</h1>';
my $htmlfooter = '<p>bestellingen script gemaakt door Ferdi Scholten</p></div>
		 </body></html>';

# open connectie voor data uitwisseling met website
my $q = CGI->new;
print $q->header;

# vul de hash %data met de gegevens van het formulier
foreach $key ($q->param()){    
	$data{$key} = $q->param($key);
	}
# validatie van ingevulde gegevens
$formname = $data{'formset'};
if ($formname eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen formset ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$jaar = $data{'jaar'};
if ($jaar eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen jaar ontvangen</p>";
     print $htmlfooter;
     exit;
     }
$week = $data{'weeknummer'};
if ($week eq "") {
     print $htmlheader;
     print $foutmelding;
     print "<p>geen weeknummer ontvangen</p>";
     print $htmlfooter;
     exit;
     }

$dirname = $formname;
$dirname =~ tr/ /_/;                                                      #vervang spaties door _
$path = "$path$dirname/$jaar/$week/";

if ( path($path)->exists ){                                               # contoleer of het geconstrueerde pad bestaat
  @bestanden = path($path)->children;                                     # haal de bestandenlijst op
  @gesorteerd = sort { substr($a, -20) cmp substr($b, -20)  } @bestanden; # sorteer de bestandenlijst op invuldatum/tijd (laatste 20 karakters van bestandsnaam)
    foreach my $bestand (@gesorteerd) {                                   # haal de inhoud op van alle bestanden in het geconstrueerde pad
      $data = path($bestand)->slurp_utf8;
      push @uitvoer, $data;
    }
}
else {print $htmlheader;
     print "<h2>Melding</h2>
            <p>Er zijn geen bestellingen gevonden voor <strong>$formname</strong> voor week $week van $jaar</p>";
     print $htmlfooter;
     exit;
     }
# creëer de webpagina om het resultaat weer te geven
print $htmlheader;
print "<h2>Succes!</h2>
       <p>Uw invoer is succesvol verwerkt, hieronder de bestellingen voor <strong>$formname</strong>, van week $week voor het jaar $jaar</p>";
foreach my $regel (@uitvoer) {
print $regel; }
print '<p>U kunt deze bestelling printen of opslaan door op onderstaande knop te klikken/tikken<br>
       <input type="button" value="Bestelling printen/opslaan" onClick="window.print()"></p>';
print $htmlfooter;       