use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);
use Data::Dumper qw(Dumper); # voor debugging om eenvoudig de inhoud van hashes etc. te bekijken
use JSON::MaybeXS qw(encode_json decode_json);

my %data;					# hash voor data vanaf de website
my $key;						# namen van de ingevulde velden vanuit de website
my $actie;
my $aanmeldingen;			#de inhoud van het JSON array bestaande uit objecten
my $aanmelding;			#de JSON objecten van 1 aanmelding vanaf de website die toegevoegd moet worden aan het json array
my $pad = "/mnt/data/data/aanmelden/";	#pad naar het bestand met het json array
my $jsonfile;				#scalar voor de naam van het bestand met het json array
my $backup;					#scalar voor backup van het originele json bestand
my $event;					#scalar voor de naam van het evenement
my $beheercode;			#scalar voor opslag van beheercode
my $codeokay;				#uitkomst controle beheercode

# open connectie voor data uitwisseling met website
my $q = CGI->new;
print $q->header;

# vul de hash %data met de gegevens van het formulier
foreach $key ($q->param()){    
	$data{$key} = $q->param($key);
	}
#bepaal wat er gedaan moet worden aan de hand van de inhoud van key actie en event
$actie = $data{'actie'};
$event = $data{'event'};
if ($event eq ""){fout("FOUT: Geen event ontvangen")}

$jsonfile = "$pad$event.json";	#het bestand waar de aanmeldingen in staan

if ($actie eq ""){fout("FOUT: Geen actie ontvangen")}
if ($actie eq "opslaan") {
	$backup = "$pad$event.bak";
	$aanmelding = $data{'aanmelding'};
	if ($aanmelding eq ""){fout("FOUT: geen aanmelding ontvangen")}
	toevoegen();
	print "Uw aanmelding werd succesvol verwerkt";
	}
elsif ($actie eq "ophalen"){
	$beheercode = $data{'beheercode'};
	chomp $beheercode;
	if ($beheercode eq "") {
	print '[{"activiteit":"fout","opmerkingen":"Geen beheercode ontvangen"}]';
	}
	else {
		my $testcode = path("/mnt/data/data/aanmelden/beheercode.txt")->slurp_utf8;
		if (index($testcode, $beheercode) != -1) {
			if (path($jsonfile)->exists ){						# controleer of het json bestand bestaat
				$aanmeldingen = path($jsonfile)->slurp_utf8;
				print $aanmeldingen;
				exit
				}
			else {
				print '[{"activiteit":"fout","opmerkingen":"Bestand met aanmeldingen niet gevonden"}]';
				}	
			}
		else {
			print '[{"activiteit":"fout","opmerkingen":"Onjuiste beheercode ontvangen"}]';
			}
		}
	}	
else {
	fout("FOUT: Onbekende actie ontvangen")
	}

sub toevoegen {
	if (path($jsonfile)->exists ){						# controleer of het json bestand bestaat
		path($jsonfile)->copy($backup);					# maak een backup van het json bestand
		my $jsonArray = path($jsonfile)->slurp_utf8;	# lees de inhoud van het json bestand
		$aanmeldingen = decode_json($jsonArray);		# converteer de inhoud naar een perl variabele (een array met hashes)
		my $aanvulling = decode_json($aanmelding);	# converteer de nieuwe aanmelding naar een perl variabele
		push @$aanmeldingen,$aanvulling;					# voeg de nieuwe aanmelding toe aan het array
		my $nieuwjson = encode_json($aanmeldingen);	# converteer het array naar json
		path($jsonfile)->spew_utf8($nieuwjson);		# schrijf het array naar het json bestand
		}
	else {														# Het json bestand werd niet gevonden.
		fout("FOUT: bestand $jsonfile met aanmeldingen niet gevonden")}
return;
}

sub fout {
	my ($foutmelding) = @_;
	print $foutmelding;
	exit 1;
return;	
}

#sub testcode {
#	$beheercode = $data{'beheercode'};
#	chomp $beheercode;
#	my $codefile = "beheercode.txt";
#	my $testcode = path($pad$codefile)->slurp_utf8;
#	if (index($testcode, $beheercode) != -1) {
#		$codeokay = 1;}
#   else {
#   	$codeokay = 0;}
#return $codeokay
#}
