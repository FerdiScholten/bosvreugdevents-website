#!/usr/bin/perl

# Perl script om JSON bestelformulieren op te slaan en te verwerken
# Geschreven door Ferdi Scholten voor Bosvreugdevents, februari 2024
# modules: CGI - https://metacpan.org/pod/CGI
#          Path::Tiny - https://metacpan.org/pod/Path::Tiny
#          Data::Dumper - https://metacpan.org/pod/Data::Dumper


use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);
# use Data::Dumper qw(Dumper);  # voor debugging om eenvoudig de inhoud van hashes etc. te bekijken

# variabelen declaratie
my %data;	                  # Hash voor ophalen gegevens webformulier
# my $path = "/mnt/data/www/bosvreugdevents/bestellingen/scripts/"; # De lokatie waar de bestanden staan en/of worden opgeslagen
my $path = "/mnt/data/www/bosvreugdevents/data/"; # De lokatie waar de bestanden staan en/of worden opgeslagen
my $key;                      # naam van het ingevulde formulierveld in het bestelformulier en hoeveelheid bestelde artikelen (als $data{$key})
my $dataset;                  # bij welke aanbieder hoort de ontvangen data (bv wonderbij)
my $categorie;                # prefix van de bestandsnaam in de dataset (bv honing)
my $lijst;                    # de gewijzigde lijst om op te slaan
my $data;                     # voor inlezen inhoud van bestanden

# open connectie voor data uitwisseling met website
my $q = CGI->new;
print $q->header;

# vul de hash %data met de gegevens van het formulier
foreach $key ($q->param()){    
	$data{$key} = $q->param($key);
	}
	
# validatie van ingevulde gegevens
$dataset = $data{'dataset'};
if ($dataset eq "") {
     print "geen dataset ontvangen";
     exit;
     }
$categorie = $data{'categorie'};
if ($categorie eq "") {
     print "geen categorie ontvangen";
     exit;
     }
$lijst = $data{'lijst'};
if ($lijst eq "") {
     print "geen bestellijst ontvangen";
     exit;
     }

my $json = "$path$dataset/$categorie.json";
my $backup = "$path$dataset/$categorie.bak";
# print "te schrijven bestand: $json <br>backup wordt gemaakt in $backup";
# exit;
if (path($json)->exists ){              # contoleer of het geconstrueerde pad bestaat
     path($json)->copy($backup);        # maak een backup kopie
     path($json)->spew_utf8($lijst);    # schrijf de nieuwe inhoud in het originele bestand
     print "succes";
     exit;
    }
print "bestand $path niet gevonden";
exit;
