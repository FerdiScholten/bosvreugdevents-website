#!/usr/bin/perl

# Perl script om invoer van HTML formulieren te verifieren tegen de inhoud van een extern bestand 
# Geschreven door Ferdi Scholten voor TiliaNatura Onderzoek, oktober 2023
# modules: CGI - https://metacpan.org/pod/CGI
#          Path::Tiny - https://metacpan.org/pod/Path::Tiny

use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);

# variabelen declaratie
my $path = "/mnt/data/data/"; # De lokatie waar de bestanden staan

# open connectie voor data uitwisseling
my $q = CGI->new;
print $q->header;

my @values = $q->multi_param('keywords'); # haal doorgegeven waardes op

my $teststring = "$values[0]"."\n"; # de eerste waarde is de te zoeken string inclusief newline
my $file = $values[1]; # de tweede waarde bevat de naam van het bestand waar de string in gezocht wordt
# say $file; # debug
my $location = "$path"."$file";
my $inhoud = path($location)->slurp_utf8;
if (index(lc($inhoud), lc($teststring)) != -1) {
    print "gevonden";
    }
# print "<p>bestandsnaam= $location </p>"; # debug
# print "<p>bestandinhoud= $inhoud </p>"; # debug
# print "<p>gezocht naar= $teststring</p>"; # debug

print ""; # niet gevonden