#!/usr/bin/perl

# Perl script om de inhoud van een extern bestand weer te geven en indien gevraagd 1 regel uit dit bestand te verijderen of toe te voegen.
# Geschreven door Ferdi Scholten voor TiliaNatura Onderzoek, oktober 2023
# modules: CGI - https://metacpan.org/pod/CGI
#          Path::Tiny - https://metacpan.org/pod/Path::Tiny

use strict;
use warnings;
use 5.010;
use CGI;
use Path::Tiny qw(path);

# variabelen declaratie
my $path = "/mnt/data/data/";                         # De lokatie waar de bestanden staan
my ($index , $file , $actie , $location, $code);
my @inhoud;

# open connectie voor data uitwisseling
my $q = CGI->new;
print $q->header;

my @values = $q->multi_param('keywords');             # haal doorgegeven waardes op

$actie = $values[0];                                  # de eerste waarde is de uit te voeren actie (ophalen/verwijderen)
$file = $values[1];                                   # de tweede waarde bevat de naam van het bestand waarvan de inhoud getoond moet worden als lijst in de browser
$location = "$path"."$file";                          # het volledige pad + bestand
@inhoud = path($location)->lines_utf8;                # lees de inhoud van het bestand als afzonderlijke regels in array @inhoud
if ($actie eq "ophalen") {                            # test of de inhoud van het bestand moet worden getoond
    ophalen();
    }
elsif ($actie eq "verwijderen") {
   $index = $values[2];                               # indexnummer van de te verwijderen regel in het array @inhoud
   $index = $index -1;                                # eerste nummer in array = 0, trek 1 af van het te verwijderen nummer
   splice @inhoud, $index, 1;                         # verwijder 1 regel uit het array @inhoud op positie $index
   path($location)->spew_utf8(@inhoud);               # overschrijf het bestand met bestelcodes met @inhoud
   ophalen();                                         # Stuur de veranderde inhoud naar de browser
   }
elsif ($actie eq "toevoegen") {
   $code = "$values[2]"."\n";
   foreach my $test (@inhoud) {                       # controleer of de toe te voegen code al bestaat
     if ($test eq $code) {                            # terugkoppeling naar browser
       print "gevonden";                              # code is gevonden toevoegen niet nodig
       exit;}
     }
   push @inhoud, $code;                               # code is niet gevonden, voeg toe aan @inhoud
   path($location)->spew_utf8(@inhoud);               # sla @inhoud op in het bestand
   print "";                                          # terugkoppeling naar browser
   }
else {
   print "<p>Er is iets fout gegaan, geen actie ontvangen</p>";
}

sub ophalen {
   foreach my $regel (@inhoud) {
   print "<li>$regel</li>";
   }
}