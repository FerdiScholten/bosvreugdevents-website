var SDATA;

async function dataFetch() {
	let url='https://bosvreugdevents.nl/vrienden/scripts/vrienden.json';
	const response = await fetch (url);
	SDATA = await response.json();
	return SDATA;
}

function vrienden() {
	dataFetch().then(SDATA => {
	maakKeuzelijst();});
}

function maakKeuzelijst() {
	let keuzeLijst = document.getElementById('keuzelijst');
	let lijstInhoud = '<h2>Keuzelijst</h2>';
	let divStart = '<div class="c1 c2 c3 c4">';
	let lijstEnd = '</a></div>';
	for (let x in SDATA) {
		let aStart = '<a href="#detailweergave" class="naam" onclick="detailWeergave(' + x + ')">';
		let imgTag = '<img class="pasfoto" src="pasfoto/' + SDATA[x].pasFoto + '" alt="' + SDATA[x].alt + '">';
		let spanTag = '<span>' + SDATA[x].voorNaam + ' ' + SDATA[x].achterNaam + '</span>';
		let infoRegel1 = '<p>' + SDATA[x].informatie1 + '</p>';
		let infoRegel2 = '<p>' + SDATA[x].informatie2 + '</p>';
		lijstInhoud = lijstInhoud + divStart + aStart + imgTag + spanTag + infoRegel1 + infoRegel2 + lijstEnd;
		}
	keuzeLijst.innerHTML = lijstInhoud;
}

function detailWeergave(x) {
	document.getElementById("detailweergave").style.display = "block";
	document.getElementById("keuzelijst").style.display = "none";
	let dFoto = document.getElementById("detailFoto");
	let dWebsite = document.getElementById("detailWebsite");
	document.getElementById("detailNaam").innerHTML = SDATA[x].voorNaam + ' ' + SDATA[x].achterNaam;
	dFoto.src = 'pasfoto/' + SDATA[x].pasFoto;
	dFoto.alt = SDATA[x].alt;
	document.getElementById("detailInforegel1").innerHTML = SDATA[x].informatie1;
	document.getElementById("detailInforegel2").innerHTML = SDATA[x].informatie2;
	dWebsite.href = SDATA[x].webSite;
	dWebsite.innerHTML = SDATA[x].webSite;
	document.getElementById("detailEmail").innerHTML = SDATA[x].eMail;
	document.getElementById("detailPublicaties").innerHTML = SDATA[x].publicaties;
	document.getElementById("detailBeroep").innerHTML = SDATA[x].beroep;
	document.getElementById("detailLezingen").innerHTML = SDATA[x].lezingen;
}

function keuzelijstWeergave() {
	document.getElementById("detailweergave").style.display = "none";
	document.getElementById("keuzelijst").style.display = "block";
}