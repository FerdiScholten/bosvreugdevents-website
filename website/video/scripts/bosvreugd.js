var VDATA;
var tPAD = "afbeeldingen/"
var vPAD = "videos/"
function spelerFormaat() {
	let w = window.innerWidth;
	let h = window.innerHeight;
	let myVideo = document.getElementById("speler");
	if (w > 1920) {w = 1920; h=1080;}
	else if (w > 1080) {w=1080; h=720}
	else if (w > 1152) {w=1153; h=648}
	else if (w > 1024) {w=1024; h=576}
	else {w=w-20; h= w/16*9;}
	myVideo.width= w;
	myVideo.height= h;
}

function afspelen(nummer) {
	//const video = JSON.parse(vdata);
	let tekst=document.getElementById("beschrijving")
	let player=document.getElementById("speler");
	tekst.innerHTML=VDATA[nummer].desc;
	player.src=vPAD + VDATA[nummer].vNaam + '.webm';
	player.play();
}

async function videoFetch() {
	let url='https://bosvreugdevents.nl/video/scripts/videos.json';
	const response = await fetch (url);
	VDATA = await response.json();
	return VDATA;
}

function video() {
	spelerFormaat();
	videoFetch().then(VDATA => {
		maakKeuzelijst();});
}

function maakKeuzelijst() {
	let keuzeLijst = document.getElementById('keuzelijst');
	let lijstInhoud = '<p>Selecteer de video die je af wilt spelen in onderstaande lijst.</p>';
	let divStart = '<div class="c1 c2 c3 c4"><a href="#speler" onclick="afspelen(';
	let imgStart = ')"><img class="thumbnail" src="afbeeldingen/';
	let imgAlt = ' alt="';
	let imgEnd = '">';
	let divEnd = '</a></div>';
	// opbouw van de inhoud waarbij wat tussen [] staat uit de json komt]
	// divStart + [VDATA.indexnummer] + imgStart + [thumbnail] + imgAlt + [VDATA.alt] + imgEnd + [VDATA.desc] + divEnd;
	for (let x in VDATA) {
		lijstInhoud = lijstInhoud + divStart + x + imgStart + VDATA[x].vNaam + '.jpg"' + imgAlt + VDATA[x].alt + imgEnd + VDATA[x].desc + divEnd;
		}
	keuzeLijst.innerHTML = lijstInhoud;
}