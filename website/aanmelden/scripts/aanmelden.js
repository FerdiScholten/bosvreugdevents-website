const url = 'https://bosvreugdevents.nl/mailer/aanmelden2.pl?';
var AANMELDINGEN;
const telnr = /[+]{1}[0-9]{11}/;
const AANMELDING = {
	voornaam:"",
	achternaam:"",
	organisatie:"",
	telefoon:"",
	email:"",
	plaats:"",
	activiteit:"",
	product:"",
	elektriciteit:"",
	amperage:0,
	aantal_mensen:1,
	bekend:"",
	website:"",
	opmerkingen:""
}
var aInhoud = 1;

//========== Functies uitsluitend voor beheer.html ==========
//Haal informatie op van server (nodig voor aanmeldbeheer)
async function haalData() {
	let CODE = document.getElementById("beheercode").value;
	if (CODE == "") {
   	document.getElementById("resultaat").innerHTML = "Beheercode niet ingevuld, je kunt geen beheer doen.";
   	return;
   }
   document.getElementById("resultaat").innerHTML = "Uw gegevens worden gecontroleerd, Een ogenblik geduld a.u.b.";
	const zendData = new FormData();
	zendData.append("actie","ophalen");
	zendData.append("beheercode",CODE);
	zendData.append("event","OpenMicXXL3")
	try {
		const response = await fetch(url, {
			method: "POST",
			body: zendData,
			});
		AANMELDINGEN = await response.json();
		return AANMELDINGEN;
		}
	catch(fout) {
		document.getElementById('resultaat').innerHTML='<strong>FOUT</strong> ophalen is mislukt met foutcode '+fout;
	}
}

function beheerCode() {
		haalData().then(AANMELDINGEN => {
			testinvoer();});
	return;
}

function testinvoer() {
		if (AANMELDINGEN[0].activiteit == "fout") {
		document.getElementById('resultaat').innerHTML = '<strong>FOUT</strong> ophalen is mislukt, '+AANMELDINGEN[0].opmerkingen;
		}
	else {
		document.getElementById('resultaat').innerHTML = '<strong>SUCCES</strong> Geldige beheercode ingevuld';
		activeerBeheer();
		}
	return;
}

function activeerBeheer() {
	document.getElementById('beheerveld').style.display = "block";
	document.getElementById('beheercodeform').style.display = "none";
	document.getElementById("aanmeldnummer").value = aInhoud;
}

function haalAanmelding() {
	aInhoud = document.getElementById("aanmeldnummer").value;
	let aantalA = AANMELDINGEN.length-1;
	if (aInhoud > aantalA) {
		aInhoud = aantalA;
		document.getElementById("aanmeldnummer").value = aInhoud;
	}
	if (aInhoud < 1) {
		aInhoud = 1;
		document.getElementById("aanmeldnummer").value = aInhoud;
	}
	showAanmelding();
}
	
function showAanmelding() {
	let aantalA = AANMELDINGEN.length-1;
	document.getElementById("numAanmelding").innerHTML = aInhoud;
	document.getElementById("aantAanmelding").innerHTML = aantalA;
	document.getElementById("voornaamB").innerHTML = AANMELDINGEN[aInhoud].voornaam;
	document.getElementById("achternaamB").innerHTML = AANMELDINGEN[aInhoud].achternaam;
	document.getElementById("organisatieB").innerHTML = AANMELDINGEN[aInhoud].organisatie;
	document.getElementById("telefoonB").innerHTML = AANMELDINGEN[aInhoud].telefoon;
	document.getElementById("emailB").innerHTML = AANMELDINGEN[aInhoud].email;
	document.getElementById("plaatsB").innerHTML = AANMELDINGEN[aInhoud].plaats;
	document.getElementById("activiteitB").innerHTML = AANMELDINGEN[aInhoud].activiteit;
	document.getElementById("productB").innerHTML = AANMELDINGEN[aInhoud].product;
	document.getElementById("elektriciteitB").innerHTML = AANMELDINGEN[aInhoud].elektriciteit;
	document.getElementById("amperageB").innerHTML = AANMELDINGEN[aInhoud].amperage;
	document.getElementById("aantal_mensenB").innerHTML = AANMELDINGEN[aInhoud].aantal_mensen;
	document.getElementById("bekendB").innerHTML = AANMELDINGEN[aInhoud].bekend;
	document.getElementById("websiteB").innerHTML = AANMELDINGEN[aInhoud].website;
	document.getElementById("opmerkingenB").innerHTML = AANMELDINGEN[aInhoud].opmerkingen;
	document.getElementById("bladerform").style.display = "block";
}

function vorige() {
	aInhoud = document.getElementById("numAanmelding").innerHTML;
	if (aInhoud > 1) {	aInhoud--;}
	else {aInhoud = 1;}
	showAanmelding();
}

function volgende() {
	aInhoud = document.getElementById("numAanmelding").innerHTML;
	let aantalA = AANMELDINGEN.length-1;
	if (aInhoud < aantalA) { aInhoud++;}
	else {aInhoud = aantalA; }
	showAanmelding();
}

function zoeken(teller) {
	aInhoud = teller;
	let zoekKnop = document.getElementById("zoekknop");
	let aantalA = AANMELDINGEN.length;
	let ikZoek = document.getElementById("zoekveld").value;
	let gevonden = "Niet gevonden";
	switch( document.getElementById("zoekCriteria").value){
	case  "voornaam": {
			for (aInhoud; aInhoud < aantalA; aInhoud++) {
			if (AANMELDINGEN[aInhoud].voornaam.includes(ikZoek)) {
				gevonden = "Gevonden";
				break;
				}
			}
		}
	break;
	case "achternaam": {
			for (aInhoud; aInhoud < aantalA; aInhoud++) {
			if (AANMELDINGEN[aInhoud].achternaam.includes(ikZoek)) {
				gevonden = "Gevonden";
				break;
				}
			}
		}
	break;
	case "organisatie": {
			for (aInhoud; aInhoud < aantalA; aInhoud++) {
			if (AANMELDINGEN[aInhoud].organisatie.includes(ikZoek)) {
				gevonden = "Gevonden";
				break;
				}
			}
		}
	}
	document.getElementById("zoekResultaat").innerHTML = gevonden;
switch(gevonden) {
	case "Gevonden":{
			showAanmelding();
				aInhoud++;
				if (aInhoud<aantalA) {
					zoekKnop.value = "Volgende zoeken";
					zoekKnop.setAttribute("onclick", "zoeken(" + aInhoud + ")");
					}
				else {
					resetZoeken();
					}
		}
		break;
	case "Niet gevonden":{
		resetZoeken();
		}
	}	

}
function resetZoeken() {
	let zoekKnop = document.getElementById("zoekknop");
	zoekKnop.value = "Zoeken";
	zoekKnop.setAttribute("onclick", "zoeken(0)");
//	document.getElementById("zoekResultaat").innerHTML = "";
}		

//========== Functies uitsluitend voor aanmelden.html ==========
//Verzend ingevulde formulierinformatie naar server (nodig voor aanmelden)
async function senddata() {
	document.getElementById("result").style.display = "block";
	document.getElementById("knoppen").style.display = "none";
	document.getElementById("resultaat").innerHTML = "Gegevens worden verstuurd";
	let aanmelding = JSON.stringify(AANMELDING);
	const formData = new FormData();
	formData.append("actie","opslaan");
	formData.append("event","OpenMicXXL3");
	formData.append("aanmelding",aanmelding);
	try {
		const response = await fetch(url, {
			method: "POST",
			body: formData,
			});
		let responseText = await response.text();
		document.getElementById("resultaat").innerHTML = responseText;
	}
	catch(fout) {
		document.getElementById('resultaat').innerHTML='<strong>FOUT</strong> gegevens verzenden is mislukt met foutcode '+fout;
	}
}
//Als er stroom nodig is kan ook het benodigde amperage ingevuld worden.
function stroomNodig() {
	if (document.getElementById("elektriciteit").checked) {
		document.getElementById("amperage").disabled = false;
	}
	else {
		document.getElementById("amperage").disabled = true;
	}
}
//valideer de inhoud van het formulier en ken waardes toe aan de checkboxes.
function valideer() {
		if (document.getElementById("activiteit").checked) {
		document.getElementById("activiteit").value = "Ik wil een workshop/lezing geven";
		AANMELDING.activiteit = "Ik wil een workshop/lezing geven";
		document.getElementById("activiteit1").style.display = "block";
		}
	else {
		document.getElementById("activiteit").value = ""
		AANMELDING.activiteit = "";
		document.getElementById("activiteit1").style.display = "none";
		}
		if (document.getElementById("bekend").checked) {
		document.getElementById("bekend").value = "Ik ben eerder standhouder geweest";
		AANMELDING.bekend = "Ik ben eerder standhouder geweest";
		}
	else {
		document.getElementById("bekend").value = "Ik kom voor de eerste keer als standhouder";
		AANMELDING.bekend = "Ik kom voor de eerste keer als standhouder";
		}
		if (document.getElementById("elektriciteit").checked) {
		document.getElementById("elektriciteit").value = "ik heb elektriciteit nodig";
		AANMELDING.elektriciteit = "ik heb elektriciteit nodig";
		AANMELDING.amperage = document.getElementById("amperage").value;
		if (AANMELDING.amperage == "") {
			document.getElementById("amperage").focus()
			alert("Verplicht veld niet ingevuld: amperage");
			return false;}
		document.getElementById("elektriciteit1").style.display = "block";
	}
	else {
		document.getElementById("amperage").disabled = true;
		document.getElementById("amperage").value = "";
		document.getElementById("elektriciteit").value = "";
		AANMELDING.elektriciteit = "";
		AANMELDING.amperage = "";
		document.getElementById("elektriciteit1").style.display = "none";
	}

	AANMELDING.voornaam = document.getElementById("voornaam").value;
	AANMELDING.achternaam = document.getElementById("achternaam").value;
	AANMELDING.organisatie = document.getElementById("organisatie").value;
	AANMELDING.telefoon = document.getElementById("telefoon").value;
	AANMELDING.email = document.getElementById("email").value;
	AANMELDING.plaats = document.getElementById("plaats").value;
	AANMELDING.product = document.getElementById("product").value;
	AANMELDING.aantal_mensen = document.getElementById("aantal_mensen").value;
	AANMELDING.website = document.getElementById("website").value;
	AANMELDING.opmerkingen = document.getElementById("opmerkingen").value;
	if (AANMELDING.plaats == "") {
		document.getElementById("plaats").focus();
		alert("Verplicht veld niet ingevuld: soort plaats");}
	else if (!telnr.test(AANMELDING.telefoon)) {
		document.getElementById("telefoon").focus();
		alert("Verplicht veld onjuist ingevuld: telefoon\nGebruik deze notatie: +31123456789");}
	else {				
	document.getElementById("voornaam1").innerHTML = AANMELDING.voornaam;
	document.getElementById("achternaam1").innerHTML = AANMELDING.achternaam;
	document.getElementById("organisatie1").innerHTML = AANMELDING.organisatie;
	document.getElementById("telefoon1").innerHTML = AANMELDING.telefoon;
	document.getElementById("email1").innerHTML = AANMELDING.email;
	document.getElementById("plaats1").innerHTML = AANMELDING.plaats;
	document.getElementById("activiteit2").innerHTML = AANMELDING.activiteit;
	document.getElementById("product1").innerHTML = AANMELDING.product;
	document.getElementById("elektriciteit2").innerHTML = AANMELDING.elektriciteit;
	document.getElementById("amperage1").innerHTML = AANMELDING.amperage;
	document.getElementById("aantal_mensen1").innerHTML = AANMELDING.aantal_mensen;
	document.getElementById("bekend1").innerHTML = AANMELDING.bekend;
	document.getElementById("website1").innerHTML = AANMELDING.website;
	document.getElementById("opmerkingen1").innerHTML = AANMELDING.opmerkingen;
	
	document.getElementById("aanmeldformulier").style.display = "none";
	document.getElementById("ingevuld").style.display = "block";
	document.getElementById("knoppen").style.display = "block";
	document.getElementById("result").style.display = "none";
	}
return false;
}

function bewerk() {
	document.getElementById("ingevuld").style.display = "none";
	document.getElementById("knoppen").style.display = "none";
	document.getElementById("aanmeldformulier").style.display = "block";
}	