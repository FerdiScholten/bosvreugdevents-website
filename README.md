Lichtgewicht website voor het doen van bestellingen door de Bosvreugd gemeenschap bij de verschillende aanbieders.

Er wordt geen data lokaal opgeslagen, geen cookies etc.
Privacy gegarandeerd, er worden geen persoonsgegevens gevraagd noch opgeslagen voor het gebruik
Gebruikt enkel HTML, CSS, JavaScript en Perl
Werkt op elke webserver met CGI ondersteuning.
